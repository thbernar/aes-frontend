import { createRouter, createWebHistory } from "vue-router";
import ChannelsView from "../views/ChannelsView.vue";
import MappingView from "../views/MappingView.vue";
import ProfilesView from "../views/ProfilesView.vue";
import BackupView from "../views/BackupView.vue";
import DetailsView from "../views/DetailsView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "channels",
      component: ChannelsView,
    },
    {
      path: "/mapping",
      name: "mapping",
      component: MappingView,
    },
    {
      path: "/profiles",
      name: "profiles",
      component: ProfilesView,
    },
    {
      path: "/backup",
      name: "backup",
      component: BackupView,
    },
    {
      path: "/details",
      name: "details",
      component: DetailsView,
    },
  ],
});

export default router;
